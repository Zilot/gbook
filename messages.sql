-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Дек 05 2013 г., 00:08
-- Версия сервера: 5.5.32
-- Версия PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `message` varchar(200) NOT NULL,
  `email` varchar(40) NOT NULL,
  `date_posted` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `name`, `message`, `email`, `date_posted`) VALUES
(5, 'Rodrigo_Galura III', 'Hello', 'Rodrigo_Galura3rd@yahoo.com', '2013-11-25'),
(7, 'Rodrigo_Galura III', 'Hello', 'Rodrigo_Galura3rd@yahoo.com', '2013-11-25'),
(10, 'sdfssdfsdfsdf', 'sdf', 'gfddsf', '2013-11-25'),
(11, 'sdfssdfsdfsdf', 'sdf', 'gfddsf', '2013-11-25'),
(12, 'sdfssdfsdfsdf', 'sdf', 'gfddsf', '2013-11-25'),
(13, 'sdfssdfsdfsdf', 'sdf', 'gfddsf', '2013-11-25'),
(14, 'sdfssdfsdfsdf', 'sdf', 'gfddsf', '2013-11-25'),
(15, 'sdfssdfsdfsdf', 'sdf', 'gfddsf', '2013-11-25'),
(16, 'sdfssdfsdfsdf', 'sdf', 'gfddsf', '2013-11-25'),
(17, 'sdfssdfsdfsdf', 'sdf', 'gfddsf', '2013-11-25'),
(18, 'sdfssdfsdfsdf', 'sdf', 'gfddsf', '2013-11-25'),
(19, 'sdfssdfsdfsdf', 'sdf', 'gfddsf', '2013-11-25'),
(20, 'sdfssdfsdfsdf', 'sdf', 'gfddsf', '2013-11-25'),
(21, 'sdfssdfsdfsdf', 'sdf', 'gfddsf', '2013-11-25'),
(22, 'dsf', 'sdfdas', 'sdfsdf', '2013-11-25'),
(23, 'Rodrigo Galura III', 'jsdklfjlas', 'dsfjjdsk', '2013-11-25'),
(24, 'Rodrigo Galura III', 'jsdklfjlas', 'dsfjjdsk', '2013-11-25'),
(25, 'reterwerwe', 'sdfsdf', 'sdfsdf', '2013-11-25'),
(26, 'reterwerwe', 'sdfsdf', 'sdfsdf', '2013-11-25'),
(27, 'reterwerwe', 'sdfsdf', 'sdfsdf', '2013-11-25'),
(28, 'reterwerwe', 'sdfsdf', 'sdfsdf', '2013-11-25'),
(29, 'reterwerwe', 'sdfsdf', 'sdfsdf', '2013-11-25'),
(30, 'sdfsadf', 'sdfsdf', 'sdfsdf', '2013-11-25'),
(31, 'sdfsadf', 'sdfsdf', 'sdfsdf', '2013-11-25'),
(32, 'sdfsadf', 'sdfsdf', 'sdfsdf', '2013-11-25'),
(33, 'sdfsdf', 'werew', 'rtwretewrwe', '2013-11-25'),
(34, 'sdfsdf', 'werew', 'rtwretewrwe', '2013-11-25'),
(35, 'sdfsdf', 'werew', 'rtwretewrwe', '2013-11-25'),
(36, 'sdfsdf', 'werew', 'rtwretewrwe', '2013-11-25'),
(37, 'Gianl', 'KobKob', 'Gian@gmail.com', '2013-11-25'),
(38, 'Gianl', 'KobKob', 'Gian@gmail.com', '2013-11-25'),
(39, 'Gianl', 'KobKob', 'Gian@gmail.com', '2013-11-25'),
(40, 'Gianl', 'KobKob', 'Gian@gmail.com', '2013-11-25'),
(41, 'Gianl', 'KobKob', 'Gian@gmail.com', '2013-11-25'),
(42, 'Gianl', 'KobKob', 'Gian@gmail.com', '2013-11-25'),
(43, 'Gianl', 'KobKob', 'Gian@gmail.com', '2013-11-25'),
(44, 'Gianl', 'KobKob', 'Gian@gmail.com', '2013-11-25'),
(45, 'Gianl', 'KobKob', 'Gian@gmail.com', '2013-11-25'),
(46, 'Gianl', 'KobKob', 'Gian@gmail.com', '2013-11-25'),
(48, 'Gianl', 'KobKobfhskdhf', 'Gian@gmail.com', '2013-11-25'),
(49, 'Gianl', 'KobKobfhskdhf', 'Gian@gmail.com', '2013-11-25'),
(50, 'Gianl', 'KobKobfhskdhf', 'Gian@gmail.com', '2013-11-25'),
(51, 'Gianl', 'KobKobfhskdhf', 'Gian@gmail.com', '2013-11-25'),
(52, 'Gianl', 'KobKobfhskdhf', 'Gian@gmail.com', '2013-11-25'),
(53, 'Gianl', 'KobKobfhskdhf', 'Gian@gmail.com', '2013-11-25'),
(54, 'asd', 'qweasd', 'asd@asd.asd', '2013-12-05'),
(55, 'asd', 'qweasd', 'asd@asd.asd', '2013-12-05'),
(57, 'asd', 'qweasd', 'asd@asd.asd', '2013-12-05'),
(58, 'asd', 'qweasd', 'asd@asd.asd', '2013-12-05'),
(59, 'asd', 'qweasd', 'asd@asd.asd', '2013-12-05'),
(60, 'asd', 'qweasd', 'asd@asd.asd', '2013-12-05');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
